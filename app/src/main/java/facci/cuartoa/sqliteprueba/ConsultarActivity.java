package facci.cuartoa.sqliteprueba;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import facci.cuartoa.sqliteprueba.Helper.SecretariaHelper;
import facci.cuartoa.sqliteprueba.Objetos.Objetos;
import facci.cuartoa.sqliteprueba.Objetos.Utilidades;

public class ConsultarActivity extends AppCompatActivity {
    RecyclerView recyclerSecretaria;
    ArrayList<Objetos> listaSecretaria;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);

        listaSecretaria = new ArrayList<>();
        recyclerSecretaria = (RecyclerView) findViewById(R.id.recycler);

        recyclerSecretaria.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerSecretaria.setHasFixedSize(true);

        SecretariaHelper dbHelper = new SecretariaHelper(this, "bd_secretaria", null, 1);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Objetos secretaria = null;



        Cursor cursor = db.rawQuery("SELECT * FROM " + Utilidades.TABLE_NAME,null);

        while (cursor.moveToNext()){

            secretaria = new Objetos();
            secretaria.setCedula(cursor.getInt(0));
            secretaria.setNombre(cursor.getString(1));
            secretaria.setApellido(cursor.getString(2));
            secretaria.setArea(cursor.getString(3));
            secretaria.setOficina(cursor.getInt(4));
            secretaria.setFecha(cursor.getString(5));
            listaSecretaria.add(secretaria);
        }

    }
}
