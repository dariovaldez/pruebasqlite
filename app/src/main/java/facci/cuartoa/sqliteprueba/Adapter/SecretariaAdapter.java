package facci.cuartoa.sqliteprueba.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import facci.cuartoa.sqliteprueba.Objetos.Objetos;
import facci.cuartoa.sqliteprueba.R;

public class SecretariaAdapter extends RecyclerView.Adapter<SecretariaAdapter.SecretariaHolder> {

    List<Objetos> listaSecretarias;

    @NonNull
    @Override
    public SecretariaHolder onCreateViewHolder(ViewGroup parent, int i) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.listasecretarias,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        vista.setLayoutParams(layoutParams);
        return new SecretariaHolder(vista);
    }

    @Override
    public void onBindViewHolder(SecretariaHolder secretariaHolder, int position) {
        secretariaHolder.tvNombre.setText(listaSecretarias.get(position).getNombre());
        secretariaHolder.tvApellidos.setText(listaSecretarias.get(position).getApellido());
        secretariaHolder.tvCedula.setText(listaSecretarias.get(position).getCedula());
        secretariaHolder.tvArea.setText(listaSecretarias.get(position).getArea());
        secretariaHolder.tvOficina.setText(listaSecretarias.get(position).getOficina());
        secretariaHolder.tvFecha.setText(listaSecretarias.get(position).getFecha());

    }

    @Override
    public int getItemCount() {
        return listaSecretarias.size();
    }

    public class SecretariaHolder extends RecyclerView.ViewHolder{

        TextView tvNombre, tvApellidos, tvCedula, tvArea, tvOficina, tvFecha;

        public SecretariaHolder(@NonNull View itemView) {
            super(itemView);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombreSecretaria);
            tvApellidos = (TextView) itemView.findViewById(R.id.tvApellidoSecretaria);
            tvCedula = (TextView) itemView.findViewById(R.id.tvCedulaSecretaria);
            tvArea = (TextView) itemView.findViewById(R.id.tvArea);
            tvOficina = (TextView) itemView.findViewById(R.id.tvOficina);
            tvFecha = (TextView) itemView.findViewById(R.id.tvFecha);
        }
    }
}
