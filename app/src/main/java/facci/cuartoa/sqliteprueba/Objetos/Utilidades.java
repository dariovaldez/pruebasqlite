package facci.cuartoa.sqliteprueba.Objetos;

public class Utilidades {


    public static final String TABLE_NAME = "secretaria";
    public static final String CEDULA = "cedula";
    public static final String NOMBRE = "nombre";
    public static final String APELLIDOS = "apeliidos";
    public static final String AREA = "area";
    public static final String OFICINA = "oficina";
    public static final String FECHA = "fecha";


    public static final String CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + " ( " + CEDULA + " INTEGER PRIMARY KEY, "
            + NOMBRE + " TEXT, " + APELLIDOS + " TEXT, " + AREA + " TEXT, " + OFICINA + " INTEGER, " + FECHA + " TEXT )";

    public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

}
