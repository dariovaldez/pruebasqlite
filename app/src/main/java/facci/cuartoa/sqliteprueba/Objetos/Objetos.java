package facci.cuartoa.sqliteprueba.Objetos;

public class Objetos {

    private int cedula;
    private String nombre;
    private String apellido;
    private String area;
    private int oficina;
    private String fecha;

    public Objetos() {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.area = area;
        this.oficina = oficina;
        this.fecha = fecha;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getOficina() {
        return oficina;
    }

    public void setOficina(int oficina) {
        this.oficina = oficina;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
