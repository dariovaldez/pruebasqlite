package facci.cuartoa.sqliteprueba;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLData;

import facci.cuartoa.sqliteprueba.Helper.SecretariaHelper;
import facci.cuartoa.sqliteprueba.Objetos.Utilidades;

public class MainActivity extends AppCompatActivity {

    EditText txtNombre, txtCedula, txtApellido, txtArea, txtOficina, txtFecha;
    Button btnIngresar, btnConsultar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCedula = findViewById(R.id.txtCedula);
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellidos);
        txtArea = findViewById(R.id.txtArea);
        txtOficina = findViewById(R.id.txtNOficina);
        txtFecha = findViewById(R.id.txtFechaNac);
        btnIngresar = findViewById(R.id.btnIngresarDato);
        btnConsultar = findViewById(R.id.btnConsultarDatos);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarDatos();
            }
        });

        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ConsultarActivity.class);
                startActivity(intent);
            }
        });

    }

    private void ingresarDatos() {

        SecretariaHelper dbHelper = new SecretariaHelper(this, "bd_secretaria", null, 1);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Utilidades.CEDULA, txtCedula.getText().toString());
        values.put(Utilidades.NOMBRE, txtNombre.getText().toString());
        values.put(Utilidades.APELLIDOS, txtApellido.getText().toString());
        values.put(Utilidades.AREA, txtArea.getText().toString());
        values.put(Utilidades.OFICINA, txtOficina.getText().toString());
        values.put(Utilidades.FECHA, txtFecha.getText().toString());

        db.insert(Utilidades.TABLE_NAME, null, values);
        db.close();
        Toast.makeText(this, "Se ingresó correctamente", Toast.LENGTH_SHORT).show();
    }
}
