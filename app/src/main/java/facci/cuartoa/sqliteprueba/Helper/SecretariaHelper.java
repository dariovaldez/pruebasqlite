package facci.cuartoa.sqliteprueba.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import facci.cuartoa.sqliteprueba.MainActivity;
import facci.cuartoa.sqliteprueba.Objetos.Utilidades;

public class SecretariaHelper extends SQLiteOpenHelper {

    public SecretariaHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilidades.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Utilidades.DELETE_TABLE);
        onCreate(db);
    }
}
